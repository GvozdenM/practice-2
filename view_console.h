
#ifndef _VIEW_CONSOLE_H_
#define _VIEW_CONSOLE_H_

#include <stdio.h>
#include <string.h>

#include "LS_allegro.h"

//general defines
#define TRUE 1
#define FALSE 0
#define CRITICAL_ERROR 9999
#define INPUT_BUFFER 1024
#define PLAYER_NAME_BUFFER INPUT_BUFFER

//view defines
#define MAIN_MENU_STATE 1
#define GAME_MENU_STATE 2
#define MENU_NAME_BUFFER 11
#define MENU_BODY_BUFFER 256

//cosmetics
#define HEADER_STYLE "------------------"
#define FOOTER_STYLE "\n------------------------------------\n"
#define MAIN_MENU_NAME "JEWELS"
#define MAIN_MENU_BODY "\t1. Play Game\n \t2. Show Rankings\n \t3. Exit\nOption: "
#define GAME_MENU_NAME "PLAY GAME"
#define GAME_MENU_BODY "\t1. New Game\n \t2. Load Game\n \t3. Exit\nOption: "

struct consoleViewMenu {
	int state;
	char name[MENU_NAME_BUFFER];
	char body[MENU_BODY_BUFFER];
};


void viewConsolePrintError(char error[], int error_type);

void viewConsolePrintMainMenu(struct consoleViewMenu *current_console_view);

void viewConsolePrintGameMenu(struct consoleViewMenu *current_console_view);

#endif