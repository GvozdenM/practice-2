#include "model.h"

#define TARGET_FIELD game_model->board.field

/*********************
 * @objective this function increments the score
 * @param game_model
 */
void modelUpdateScore(struct Game *game_model) {
    game_model->score++;
}

/**********************************************
 * @objective this function resets the game
 * field
 * @param game_model
 */
void modelResetGame(struct Game *game_model) {
    int i = 0, j = 0;

    for(i = 0; i < FIELD_HEIGHT_BX; i++) {
        for(j = 0; j < FIELD_WIDTH_BX ; j++) {
            TARGET_FIELD[i][j].colour = 0;
        }
    }

    game_model->score = 0;
    game_model->game_time = 0;
}


/**********************************************
 * @objective this function moves all elements
 * which are floating downwards
 * @param game_model
 * @return
 */
int modelUpdateElements(struct Game *game_model) {
    int i = 0, j = 0, k = 0;

    int updated = TRUE;

    elementModel temp_element = {0, 0, 0, 0, 0};


    for(j = 0; j < FIELD_WIDTH_BX; j++) {

        /*sorting a row*/
        for(i = FIELD_HEIGHT_BX - 1; i >= 3; i--) {
            for(k = FIELD_HEIGHT_BX - 1; k >= 3; k--) {

                if(TARGET_FIELD[k][j].colour == 0) {

                    temp_element.colour = TARGET_FIELD[k - 1][j].colour;
                    temp_element.mark_for_deletion = TARGET_FIELD[k - 1][j].mark_for_deletion;

                    TARGET_FIELD[k - 1][j].colour = TARGET_FIELD[k][j].colour;
                    TARGET_FIELD[k - 1][j].mark_for_deletion = TARGET_FIELD[k][j].mark_for_deletion;

                    TARGET_FIELD[k][j].colour = temp_element.colour;
                    TARGET_FIELD[k][j].mark_for_deletion = temp_element.mark_for_deletion;

                    if(temp_element.colour != 0) updated = TRUE;
                }
            }
        }
    }
    return updated;
}


/*************************************************************
 * @objective this function searches for links on the board
 * vertically and marks balls in a link to be delted
 * @param game_model
 * @return
 */
int pModelFindVerticalLinks(struct Game *game_model) {
    int i = 0, j = 0;

    int links_found = FALSE;

    for(j = 0; j < FIELD_WIDTH_BX; j++) {

        //loop to check for vertical links of 3
        for(i = 3; i < FIELD_HEIGHT_BX - 2; i++) {

            if(TARGET_FIELD[i][j].mark_for_deletion != TRUE && TARGET_FIELD[i][j].colour != 0) {

                if(TARGET_FIELD[i][j].colour == TARGET_FIELD[i + 1][j].colour &&
                    TARGET_FIELD[i][j].colour == TARGET_FIELD[i + 2][j].colour) {

                    TARGET_FIELD[i][j].mark_for_deletion =
                            TARGET_FIELD[i + 1][j].mark_for_deletion =
                                    TARGET_FIELD[i + 2][j].mark_for_deletion = TRUE;

                    links_found = TRUE;
                    modelUpdateScore(game_model);
                }
            }
        }
    }

    return links_found;
}

/*********************************************************
 * @objective This function serches for horizontal links
 * within the field of the given game model. When a link
 * is found the function marks linked balls for deletion
 * @param game_model
 * @return
 */
int pModelFindHorizontalLinks(struct Game *game_model) {
    int i = 0, j = 0;

    int links_found = FALSE;

    for(i = 0; i < FIELD_HEIGHT_BX; i++) {
        for(j = 0; j < FIELD_WIDTH_BX - 2; j++) {

            if(TARGET_FIELD[i][j].mark_for_deletion != TRUE && TARGET_FIELD[i][j].colour != 0) {

                if(TARGET_FIELD[i][j].colour == TARGET_FIELD[i][j + 1].colour &&
                    TARGET_FIELD[i][j].colour == TARGET_FIELD[i][j + 2].colour) {

                    TARGET_FIELD[i][j].mark_for_deletion =
                            TARGET_FIELD[i][j + 1].mark_for_deletion =
                                    TARGET_FIELD[i][j + 2].mark_for_deletion = TRUE;

                    links_found = TRUE;
                    modelUpdateScore(game_model);
                }
            }
        }
    }

    return  links_found;
}

/********************************************************
 * This function loops through each position in the game
 * field matrix and determins whether with respect to
 * it any links were found. Searching for links is
 * abstracted away into two function calls.
 *
 * @param game_model
 * @return
 */
int modelMarkLinks(struct Game *game_model) {
    int i = 0, j = 0, k = 0, l = 0;

    int links_found = FALSE;

    for(i = FIELD_HEIGHT_BX - 1; i >= 3; i--) {
        for(j = 0; j < FIELD_WIDTH_BX - 1; j++) {

            //checking for vertical links

            k = i;
            if(TARGET_FIELD[k][j].colour != 0) {
                if(pModelFindVerticalLinks(game_model)) links_found = TRUE;
                if(pModelFindHorizontalLinks(game_model)) links_found = TRUE;
                //pModelFindDiagonalLinks(game_model, i, j);
            }
        }
    }

    return links_found;
}

/**************************************************************
 * @objective This function removes from the game field those
 * balls which have been marked for deletion via the
 * variable mark for deletion
 * @param game_model
 */
void modelRemoveMarkedBalls(struct Game *game_model) {
    int i = 0, j = 0;

    for(i = 0; i < FIELD_HEIGHT_BX; i++) {
        for(j = 0; j < FIELD_WIDTH_BX; j++) {
            if(game_model->board.field[i][j].mark_for_deletion == TRUE) {
                game_model->board.field[i][j].colour = 0;
                game_model->board.field[i][j].mark_for_deletion = FALSE;
            }
        }
    }
}

/***********************************************************
 * @objective Given a seed this function
 * generates a random number using the integrated
 * rand() function. The generated numbers are within
 * the given range of colours which are designated by
 * the design.
 *
 * @param seed
 * @return
 */
int pModelGetRandomNumber(unsigned seed) {
    srand(seed);

    int rand_num = (rand() % NUM_COLOURS);

    rand_num += rand_num < 1 ? 1 : 0;

    return rand_num;
}

/**************************************************************
 * @objectives This function serves the purpose of generating
 * a group of balls which will be falling down the screen
 * and responding to user input. This group need have random
 * colours assigned to each ball and as such this function
 * utilises pModelGetRandomNumber
 * @param game_model
 * @param seed
 */
void modelGenerateNewHead(struct Game *game_model, unsigned seed) {
    int i = 0;
    
    movingElementModel new_element;

    new_element.cluster[0].y = 2;
    
    new_element.mark_for_deletion = FALSE;
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
        
        new_element.cluster[i].colour = pModelGetRandomNumber(seed + i);

        new_element.cluster[i].type = OBJECT_TYPE_CIRCLE;

        new_element.cluster[i].mark_for_deletion = 0;
        

        if(i > 0) new_element.cluster[i].y = new_element.cluster[i - 1].y - 1;
        
        new_element.cluster[i].x = FIELD_WIDTH_BX / 2;
        
        //TOOD maybe ad switch to get type from colour
    }
    
    game_model->board.moving_element = new_element;
}

/*************************************************************
 * @objectives The purpose of this function is to shift the
 * group of moving balls to the opposite side of the screen
 * if the user attempts to exit out of the screen by moving
 * either too far left or too far right
 * @param current_element
 */
void modelElementMirror(movingElementModel *current_element) {
    int i = 0;
    int state = 0;
    
    if(current_element->cluster[0].x <= 1) {
        state = 1;
    }
    else {
        state = 2;
    }
    
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) { 
        switch(state) {
            case 1:
                current_element->cluster[i].x = FIELD_WIDTH_BX;
                break;
            case 2:
                current_element->cluster[i].x = 1;
                break;
        default:break;
        }
    }
}

/**********************************************************
 * @objective This function sorts the order of balls in the
 * moving group as per the requirments of the projecct.
 * @param head
 */
void modelHeadFlip(movingElementModel *head) {
    int i = 0, j = 0;
    int temp;
    
    /*looping through array of moving elements*/
    for(i = 0; i < NUM_MOVING_ELEMENTS - 1; i++) {
       
            /*performing temp variable swap*/
            temp = head->cluster[i].colour;
            head->cluster[i].colour = head->cluster[i + 1].colour;
            head->cluster[i + 1].colour = temp;
    }
}

/******************************************************
 * @objective This function serves the purpose of moving
 * a targeted element by a certain direction in the x and
 * y directions
 * @param x
 * @param y
 * @param current_element
 */
void pModelElementMove(int x, int y, elementModel *current_element) {
    current_element->x += x;
    current_element->y += y;
}

/********************************************************
 * @objectives This function updates the position of the
 * moving and falling group of balls on the screen. The
 * function moves the balls down by one box.
 * @param head
 */
void modelHeadMoveDown(movingElementModel *head) {
    int i = 0;
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
        pModelElementMove(0, 1, &head->cluster[i]);
    }
}

/*******************************************************
 * @objectives This function updates the position of the
  * moving and falling group of balls on the screen. The
  * function moves the balls left by one box.
 * @param head
 */
void modelHeadMoveLeft(movingElementModel *head) {
    int i = 0;
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
        pModelElementMove(-1, 0, &head->cluster[i]);
    }
}

/*****************************************************
 * @objectives This function updates the position of the
  * moving and falling group of balls on the screen. The
  * function moves the balls right by one box.
 * @param head
 */
void modelHeadMoveRight(movingElementModel *head) {
    int i = 0;
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
        pModelElementMove(1, 0, &head->cluster[i]);
    }    
}


/************************************************************
 * @objective This function saves the group of moving balls
 * in to the game field matrix. This makes them static. The
 * function is only intended to be called when a sticking
 * condition (i.e. bottome edge of screen or ball contact)
 * is made
 * @param target_game
 */
void modelHeadMakeStatic(struct Game *target_game) {
    int i = 0;
    
    for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
        target_game->board.field[target_game->board.moving_element.cluster[i].y - 1]
        [target_game->board.moving_element.cluster[i].x - 1] =
            target_game->board.moving_element.cluster[i];
    }
}

void modelElementMoveDown(elementModel *element) {
    pModelElementMove(0, 1, element);
}

void modelElementMoveLeft(elementModel *element) {
    pModelElementMove(-1, 0, element);
}

void modelElementMoveRight(elementModel *element) {
    pModelElementMove(1, 0, element);
}

