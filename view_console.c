#include "view_console.h" 

void pViewConsoleSetViewState(struct consoleViewMenu *console_view, int requested_state) {
    console_view->state = requested_state;
}

void pViewConsoleSetViewBody(struct consoleViewMenu *console_view, const char new_body[MENU_BODY_BUFFER]) {
    int i = 0;

    for(i = 0; i < MENU_BODY_BUFFER; i++) {
        console_view->body[i] = new_body[i];
    }
}

void pViewConsoleSetViewName(struct consoleViewMenu *console_view, const char new_name[MENU_NAME_BUFFER]) {
    int i = 0;

    for(i = 0; i < MENU_NAME_BUFFER; i++) {
        console_view->name[i] = new_name[i];
    }
}

void pViewConsolePrint(char input[]) {
    printf("%s", input);
}

void viewConsolePrintError(char error[], int error_type) {
	printf("ERROR Code %d:", error_type);
    pViewConsolePrint(error);
}

void pViewConsoleShowHeader(const char menu_name[MENU_NAME_BUFFER]) {
    printf("%s%s%s\n", HEADER_STYLE, menu_name, HEADER_STYLE);
}

void pViewConsoleShowFooter() {
    printf("%s", FOOTER_STYLE);
}


void pViewConsoleShowMenu(struct consoleViewMenu *console_view) {
    LS_allegro_console_clear_screen();

    pViewConsoleShowHeader(console_view->name);
    printf("%s", console_view->body);
    pViewConsoleShowFooter();
}

void pViewConsoleMenuHandler(struct consoleViewMenu *current_console_view, int requested_state) {

	if (requested_state == current_console_view->state) {
	    pViewConsoleShowMenu(current_console_view);
	}

	else {
		switch(requested_state) {
			case MAIN_MENU_STATE:
				//LS_allegro_console_clear_screen();

				/*setting console view object state to main menu state*/
                pViewConsoleSetViewBody(current_console_view, MAIN_MENU_BODY);
                pViewConsoleSetViewName(current_console_view, MAIN_MENU_NAME);
                pViewConsoleSetViewState(current_console_view, MAIN_MENU_STATE);

                /*printing constructed menu*/
                pViewConsoleShowMenu(current_console_view);
                break;

		    case GAME_MENU_STATE:
                pViewConsoleSetViewBody(current_console_view, GAME_MENU_BODY);
                pViewConsoleSetViewName(current_console_view, GAME_MENU_NAME);
                pViewConsoleSetViewState(current_console_view, GAME_MENU_STATE);

                pViewConsoleShowMenu(current_console_view);
                break;
		    default:
		        viewConsolePrintError("INVALID MENU STATE!", CRITICAL_ERROR);
		        break;
		}
	}
}

void viewConsolePrintMainMenu(struct consoleViewMenu *current_console_view) {
	pViewConsoleMenuHandler(current_console_view, MAIN_MENU_STATE);
}

void viewConsolePrintGameMenu(struct consoleViewMenu *current_console_view) {
    pViewConsoleMenuHandler(current_console_view, GAME_MENU_STATE);
}



