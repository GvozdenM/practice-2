#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "view_console.h"
#include "view.h"
#include "model.h"

//general defines
#define TRUE 1
#define FALSE 0
#define INPUT_ERROR 1
#define FILE_READ_ERROR 3
#define CRITICAL_ERROR 9999

//controller defines
#define REQUEST_PLAYGAME 1
#define REQUEST_RANKINGS 2
#define REQUEST_EXIT 3
#define REQUEST_NEWGAME 4
#define REQUEST_LOADGAME 5
#define REQUEST_UNKNOWN 6

#define CONTROLLER_STATE_MAIN_MENU 1
#define CONTROLLER_STATE_GAME_MENU 2
#define CONTROLLER_STATE_ENTER_GAME 3
#define CONTROLLER_STATE_IN_GAME 4
#define CONTROLLER_STATE_EXITING_GAME 5
#define CONTROLLER_STATE_EXITED_GAME 6


//buffer defiens
#define MENU_INPUT_BUFFER 20
#define MENU_NAME_BUFFER 11
#define MENU_BODY_BUFFER 256

///quality of life defines
#define TARGET_CLUSTER game_model->board.moving_element.cluster
#define TARGET_CLUSTER_I TARGET_CLUSTER->y - 1
#define TARGET_CLUSTER_J TARGET_CLUSTER->x - 1

struct rankingMember {
    int score;
    char name[PLAYER_NAME_BUFFER];
};

struct Controller {
    int state;
    int initiated_allegro;
    char console_input[MENU_INPUT_BUFFER];
};

/**********************************************************
 * @objective this function converst a string of digits
 * represented as ascii characters into an integer.
 * @param input is a string of digits of type char all
 * characters should represent values from '0' to '9'
 * otherwise, the integer ouputs may produce unexpected
 * results.
 * @return
 */
int atoi(const char input[INPUT_BUFFER]) {
    /*local variables*/
    int integer = 0;
    int i;                                                                      // Loop counters

    /*looping through input array until input[i] is end of string*/
    for(i = 0; input[i] != '\0'; i++) {
        integer = (integer * 10) + (input[i] - '0');
    }
    return integer;                                                             //returning converted integer
}

/*********************************************************
 * @objective this function reads from the ranking file
 * raanking.txt. The ranking is read as a string
 * of characters, up to, the size of the input buffer,
 * or until the end of the file is reached.
 *
 * output_line is a pointer to the string in to which
 * the contents of the file are to be read. the function
 * can also be easily adapted to print the contents
 * directly into the console.

 * @param output_line is
 */
void pControllerReadRankings(char *output_line) {

    /*local variables*/
    FILE *game_rankings = NULL;
    char print_line[PLAYER_NAME_BUFFER + 10] = {'\0'};
    char *concat_line = NULL;
    int num_concats = 0;

    game_rankings = fopen("ranking.txt", "r");                              //opening file in read mode

    /*checking whether file has been found and opened without errors*/
    if(game_rankings != NULL) {

        /*getting a single character from the game_rankings stream
         * until the return of fgets is NULL (i.e. at EOF)*/
        while(fgets(print_line, sizeof print_line, game_rankings) != NULL) {

            /*adding character to accumulated string*/
            strcat(output_line, print_line);

            num_concats++;
        }
        if(feof(game_rankings)) {
            /*chekcing if any data has been read so far*/
            if(num_concats == 0) {
                /*if no data was read file is empty*/
                viewConsolePrintError("File was opened but empty!", 4);
            }
        }
        else{
            /*printing error message*/
            viewConsolePrintError("Unknown read error found!", 4);
        }

        /*closing file since it has been opened*/
        fclose(game_rankings);
    }

    /*file not opened due to errors or not being found*/
    else {
        viewConsolePrintError("No ranking file found!\n", FILE_READ_ERROR);
    }
}


/*************************************************************
 * @objective this function aims to segment the input by
 * changing behaviour depending on the type of data
 * separator which was last read.
 *
 * Rankings is a pointer to an array of rankingMembers
 * into which, the already read single string input from
 * a file, will be segmented and placed.
 *
 * @param rankings
 * @param ranking_string
 */
void pControllerSegmentRankings(struct rankingMember *rankings, const char ranking_string[INPUT_BUFFER]) {
    /*local variables*/
    int i = 0, e = 0, w = 0;
    int ranking_num = 0;
    int data_type = 1;
    char score[100] = {'\0'};


    /*looping through the string containing rankings*/
    for(i = 0; ranking_string[i] != '\0'; i++) {

        /*checking whether current char is a ranking separator*/
        if(ranking_string[i] == '\n') {

            /*updating counters and pointers*/
            ++rankings;                                                         //moving forward in array by one mem address
            data_type = 1;
            e = 0;

            /*clearing data in score char array*/
            for(w = 0; w < 100; w++) {
                score[w] = '\0';
            }
        }
        else {
            /*checking whether current char is ranking data separator*/
            if(ranking_string[i] == '-') {

                /*updating counter*/
                data_type = 2;
                e = 0;
            }

            else {
                /*if last read separator was of type ranking separator*/
                if(data_type == 1) {
                    /*storing current char in name array*/
                    rankings->name[e] = ranking_string[i];
                    e++;
                }
                else {
                    /*storing current char in score array*/
                    score[e] = ranking_string[i];
                    e++;
                    /*converting char array to integer*/
                    rankings->score = atoi(score);
                }
            }
        }
    }
}

/***************************************************
 * @objective This function serves the purpose
 * of abstracting away the routing for reading
 *
 *
 *
 * @param rankings
 */
void pControllerLoadRankings(struct rankingMember *rankings) {
    char ranking[INPUT_BUFFER] = {'\0'};

    pControllerReadRankings(ranking);

    pControllerSegmentRankings(rankings, ranking);

}

void pControllerSortRankings() {
    int i = 0, j;
    struct rankingMember rankings[800] = {
            {0},{'\0'}
    };
    struct rankingMember temp;

    pControllerLoadRankings(rankings);

    for(i = 0; i < 800; i++) {
            for(j = 0; j < 799; j++) {
                if(rankings[j].score < rankings[j + 1].score) {
                    temp = rankings[j + 1];

                    rankings[j + 1] = rankings[j];

                    rankings[j] = temp;
                }
            }
        }

    for(i = 0; i < 10; i++) {
        printf("Name: %s \t Score: %d\n", rankings[i].name, rankings[i].score);
    }

    //exit(0);
}

void pControllerWriteToRankings(struct Game game_model) {
    FILE * game_rankings = NULL;

    game_rankings = fopen("ranking.txt", "a+");

    if(game_rankings != NULL) {
        fprintf(game_rankings, "%s-%d\n", game_model.player_name, game_model.score);
        fclose(game_rankings);
    }

    else {
        viewConsolePrintError("No ranking file found!", FILE_READ_ERROR);
    }

}

int controllerLoadGame(struct Game *game_model) {
    FILE * game_save = NULL;
    game_save = fopen("game.bin", "r+");

    if(game_save != NULL) {
        fread(game_model, sizeof(struct Game), 1, game_save);
        fclose(game_save);
        return TRUE;
    }
    else {
        viewConsolePrintError("No game file found!\n", FILE_READ_ERROR);
        return FALSE;
    }
}

void controllerSaveGame(struct Game *game_model) {
    FILE * game_save = NULL;

    game_save = fopen("game.bin", "wb+");

    if(game_save != NULL) {
        fwrite(game_model, sizeof(struct Game), 1, game_save);
        fclose(game_save);
    }
}

void pControllerPauseGame() {
    float x = 0;

    float center_x = (VIEW_WIDTH / 2);
    float center_y = (VIEW_HEIGHT / 2);

    while(!LS_allegro_key_pressed(ALLEGRO_KEY_P)) {
        al_flip_display();
           viewDrawText("PAUSED", center_x, center_y);
    }
}

int controllerCheckFailureCondition(struct Board *board_model) {
    int i = 0;

    for(i = 0; i <= FIELD_WIDTH_BX - 1; i++) {
        if(board_model->field[2][i].colour != FALSE) return 1;
    }

    return 0;
}

void controllerSetState(struct Controller *game_controller, int new_state) {
    game_controller->state = new_state;
}

void pControllerGetInput(char user_console_input[INPUT_BUFFER]) {
    //counters
    int i = 0;

    //clearing previous input
    for(i = 0; i < MENU_INPUT_BUFFER; i++) user_console_input[i] = '\0';

    //getting new input
    fgets(user_console_input, MENU_INPUT_BUFFER, stdin);

    for(i = 0; i < strlen(user_console_input); i++) {
        if(user_console_input[i] == '\n') {
            user_console_input[i] = '\0';
        }
    }
}

int pControllerInterpretConsoleInput(struct Controller *game_controller) {

    if(strcmp(game_controller->console_input, "Exit") == 0 ||
    strcmp(game_controller->console_input, "3") == 0) return REQUEST_EXIT;


    switch(game_controller->state) {
        /*handling controller in main menu state*/
        case CONTROLLER_STATE_MAIN_MENU:
            if(strcmp(game_controller->console_input, "Play Game") == 0 ||
            strcmp(game_controller->console_input, "1") == 0) return REQUEST_PLAYGAME;

            if(strcmp(game_controller->console_input, "Show Ranking") == 0 ||
            strcmp(game_controller->console_input, "2") == 0) return REQUEST_RANKINGS;
            break;

        /*handling controller in game menu state*/
        case CONTROLLER_STATE_GAME_MENU:
            if(strcmp(game_controller->console_input, "New Game") == 0 ||
            strcmp(game_controller->console_input, "1") == 0) return REQUEST_NEWGAME;

            if(strcmp(game_controller->console_input, "Load Game") == 0 ||
            strcmp(game_controller->console_input, "2") == 0) return REQUEST_LOADGAME;
            break;

        default:
            break;
    }
    return REQUEST_UNKNOWN;
}

int controllerEnterGameMenu(struct Controller *game_controller, struct consoleViewMenu *console_view) {
    //counters and triggers
    int request = REQUEST_UNKNOWN;
    int i = 0;

    controllerSetState(game_controller, CONTROLLER_STATE_GAME_MENU);

    for(i = 0; i < MENU_INPUT_BUFFER; i++) {
        game_controller->console_input[i] = '\0';
    }

    while(request == REQUEST_UNKNOWN) {
        //telling view to print the Game Menu to the console
        viewConsolePrintGameMenu(console_view);

        //getting console_input from user
        pControllerGetInput(game_controller->console_input);

        //telling interpreter to process the user input and return a request value
        request = pControllerInterpretConsoleInput(game_controller);
    }

    return request;
}

int controllerEnterMainMenu(struct Controller *game_controller, struct consoleViewMenu *console_view) {
    //counters and triggers
    int request = REQUEST_UNKNOWN;
    int i = 0;

    //setting controller to requested state
    controllerSetState(game_controller, CONTROLLER_STATE_MAIN_MENU);

    for(i = 0; i < MENU_INPUT_BUFFER; i++) {
        game_controller->console_input[i] = '\0';
    }

    while(request == REQUEST_UNKNOWN) {

        //telling view to print the MAIN MENU to the console
        viewConsolePrintMainMenu(console_view);

        //getting console_input from user
        pControllerGetInput(game_controller->console_input);

        //telling interpreter to process the user input and return a request value
        request = pControllerInterpretConsoleInput(game_controller);
    }

    return request;
}


void pControllerTriggerLeftKeyEvent(struct Game *game_model) {

    if(TARGET_CLUSTER->x > 1 && game_model->board.field[TARGET_CLUSTER_I][TARGET_CLUSTER_J - 1].colour == 0) {
        modelHeadMoveLeft(&game_model->board.moving_element);
    }

    else {
        TARGET_CLUSTER->x <= 1 ? modelElementMirror(&game_model->board.moving_element) : NULL;
    }
}

void pControllerTriggerRightKeyEvent(struct Game *game_model) {


    if(TARGET_CLUSTER->x < FIELD_WIDTH_BX && game_model->board.field[TARGET_CLUSTER_I][TARGET_CLUSTER_J + 1].colour == 0) {
        modelHeadMoveRight(&game_model->board.moving_element);
    }

    else {
        TARGET_CLUSTER->x >= FIELD_WIDTH_BX ? modelElementMirror(&game_model->board.moving_element) : NULL;
    }
}

void pControllerTriggerSpaceKeyEvent(struct Game *game_model) {
    modelHeadFlip(&game_model->board.moving_element);
}

void pControllerTriggerDownKeyEvent(struct Game *game_model) {
    if (TARGET_CLUSTER->y < FIELD_HEIGHT_BX &&
    game_model->board.field[TARGET_CLUSTER_I + 1][TARGET_CLUSTER_J].colour == 0) {
        /*if not touching screen bottom element is safe to move down*/
        modelHeadMoveDown(&game_model->board.moving_element);
    }
    else {
        /*if moving element is touching bottom of screen
        * making moving element static*/
        modelHeadMakeStatic(game_model);
        game_model->board.moving_element.mark_for_deletion = TRUE;

        while (modelMarkLinks(game_model)) {
            modelRemoveMarkedBalls(game_model);
            modelUpdateElements(game_model);
        }
    }
}

void pControllerTriggerSaveKeyEvent(struct Game *game_model) {
    controllerSaveGame(game_model);
}

void pControllerTriggerClockEvents(struct Game *game_model) {
    game_model->game_time++;
    pControllerTriggerDownKeyEvent(game_model);
}

/********************************************************
 * @objectives This function checks if any user input
 * has been recorded and if so makes appropriate calls
 * to trigger events for each of the given user inputs
 * such that each command's functionality can easily
 * be extended
 * @param game_model
 */
void pControllerHandleKeyEvents(struct Game *game_model) {
    /*HANDLING KEY EVENTS*/

    //triggering events if left key is pressed
    if(LS_allegro_key_pressed(ALLEGRO_KEY_LEFT)) {
        pControllerTriggerLeftKeyEvent(game_model);
    }

    //triggering events if right key is pressed
    if(LS_allegro_key_pressed(ALLEGRO_KEY_RIGHT)) {
        pControllerTriggerRightKeyEvent(game_model);
    }

    //triggering events if space key is pressed
    if(LS_allegro_key_pressed(ALLEGRO_KEY_SPACE)) {
        pControllerTriggerSpaceKeyEvent(game_model);
    }

    if(LS_allegro_key_pressed(ALLEGRO_KEY_DOWN)) {
        pControllerTriggerDownKeyEvent(game_model);
    }

    if(LS_allegro_key_pressed(ALLEGRO_KEY_S)) {
        pControllerTriggerSaveKeyEvent(game_model);
    }

    /*
    //key to reset the game (testing purposes)
    if(LS_allegro_key_pressed(ALLEGRO_KEY_R)) {
        modelResetGame(game_model);
    }*

    //key to pause the game (testing purposes)
    if(LS_allegro_key_pressed(ALLEGRO_KEY_P)) {
        LS_allegro_key_pressed(ALLEGRO_KEY_P);
                pControllerPauseGame();
    }*/
}

/**********************************************************************
 * @objective This function serves the purpose of handling the business
 * logic of the game. User inputs are handled here as are clock events.
 * The main game loops is here and all of the logic of how the game
 * responds to commands is here. However, the actual logic of how
 * the game is displayed and handles the game model are abstracted
 * into the view and model modules respectively.
 * @param game_controller
 * @param game_model
 * @return
 */
int controllerEnterGameTimer(struct Controller *game_controller, struct Game *game_model) {
    int i = 0, ticks = 0;
    unsigned rng_seed = (unsigned) (time(NULL) / 8);
    game_model->score = 0;

    float time1, time0;

    controllerSetState(game_controller, CONTROLLER_STATE_ENTER_GAME);

    viewInitiateGameWindow();
    game_controller->initiated_allegro = TRUE;

    modelGenerateNewHead(game_model, rng_seed);

    controllerSetState(game_controller, CONTROLLER_STATE_IN_GAME);


    time0 = (float) clock();

    //Starting game loop
    while(game_controller->state != CONTROLLER_STATE_EXITING_GAME) {

        //printing empty view structure
        if(ticks % 13 == 0) viewDrawDefaultBoard();

        //updating rng seed every loop cycle
        ++rng_seed;

        //Checking if ESC key has been pressed
        if(LS_allegro_key_pressed(ALLEGRO_KEY_ESCAPE)) {
            controllerSetState(game_controller, CONTROLLER_STATE_EXITING_GAME);
        }

        /*counting ticks*/
        time1 = (float) clock();
        if( (time1 - time0) / (float) CLOCKS_PER_SEC >= 1.2) {
            ++ticks;
        }

        /*checking if two full game ticks have passed, if so
        triggering on clock events*/
        if (ticks >= 3) {

            ticks = 0;

            /*handling on clock events*/
            pControllerTriggerClockEvents(game_model);

            //saving current time stamp to use in next loop cycle*/
            time0 = (float)clock();
        }

        /*checking if moving element can be overwritten*/
        if(TRUE == game_model->board.moving_element.mark_for_deletion) {
            /*if element has been statified generating new element*/
            modelGenerateNewHead(game_model, rng_seed);
        }

        /*telling view to display each element inside the moving
        element cluster*/
        for(i = 0; i < NUM_MOVING_ELEMENTS; i++) {
            if(TARGET_CLUSTER[i].y > 2) {
                viewDrawObject(game_model->board.moving_element.cluster[i]);
            }
        }

        /*printing static balls in field*/
        viewDrawField(game_model);
        viewDrawPlayerName(game_model->player_name);
        viewDrawScore(game_model->score);
        viewDrawTime(game_model->game_time);

        pControllerHandleKeyEvents(game_model);

        if(controllerCheckFailureCondition(&game_model->board) == 1) {
            controllerSetState(game_controller, CONTROLLER_STATE_EXITING_GAME);
            pControllerWriteToRankings(*game_model);
            modelResetGame(game_model);
        }
    }

    return REQUEST_EXIT;
}

/*************************************************************************
 * @objective This function routes the flow of the program based on the
 * user's input into the console. It starts the game, prints the window
 * initiates the game controller and decides which actions need be taken
 * in order to process the user's console request. However all of what
 * is done to actually serve the request to the user is abstracted
 * away into other functions.
 * @param game_controller
 * @param console_view
 * @param game_model
 */
void initiateGameController(struct Controller game_controller, struct consoleViewMenu console_view, struct Game game_model) {
    int request = -1;

    //iniciating game menu
    request = controllerEnterMainMenu(&game_controller, &console_view);

    while (game_controller.state != CONTROLLER_STATE_ENTER_GAME &&
           game_controller.state != CONTROLLER_STATE_EXITED_GAME) {

        switch (request) {

        case REQUEST_PLAYGAME:
            request = controllerEnterGameMenu(&game_controller, &console_view);
            break;

        case REQUEST_RANKINGS:
            pControllerSortRankings();
            //pControllerReadRankings(NULL);
            request = controllerEnterMainMenu(&game_controller, &console_view);
            break;

        case REQUEST_NEWGAME:

            printf("Enter your username ");
            pControllerGetInput(game_model.player_name);
            request = controllerEnterGameTimer(&game_controller, &game_model);
            break;

        case REQUEST_LOADGAME:
            if(controllerLoadGame(&game_model)) {
                request = controllerEnterGameTimer(&game_controller, &game_model);
                break;
            }
            else {
                request = controllerEnterGameMenu(&game_controller, &console_view);
            }

        case REQUEST_EXIT:

            if (game_controller.state == CONTROLLER_STATE_GAME_MENU) {
                request = controllerEnterMainMenu(&game_controller, &console_view);
            }
            else {
                if (game_controller.state == CONTROLLER_STATE_MAIN_MENU) {
                    game_controller.state = CONTROLLER_STATE_EXITED_GAME;
                    break;
                }
                else {
                    if (TRUE == game_controller.initiated_allegro) {
                        if (game_controller.state == CONTROLLER_STATE_EXITING_GAME) {
                            LS_allegro_exit();
                            game_controller.initiated_allegro = FALSE;
                            request = controllerEnterMainMenu(&game_controller, &console_view);
                        }
                    }
                }
                break;

            case REQUEST_UNKNOWN:
            default:

                viewConsolePrintError("Unknown menu option chosen!\n", INPUT_ERROR);

                if (game_controller.state == CONTROLLER_STATE_MAIN_MENU) {
                    request = controllerEnterMainMenu(&game_controller, &console_view);
                }

                else {
                    request = controllerEnterGameMenu(&game_controller, &console_view);
                }

                break;
            }
        }
    }
}

int main(void) {

    setenv("DISPLAY", "127.0.0.1:0", true);
    struct consoleViewMenu console_view = {-1, {'\0'},{'\0'}};
    struct Controller game_controller = {-1, FALSE, {'\0'}};
    struct Game game_model = {0, 0, 0, {'\0'},{}};

    initiateGameController(game_controller, console_view, game_model);

	return 0;
}
