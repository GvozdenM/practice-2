#ifndef _VIEW_H_
#define _VIEW_H_

#include "LS_allegro.h"
#include "model.h"

#define VIEW_WIDTH 720.00
#define VIEW_HEIGHT 800.00

void viewDrawField(struct Game *game_model);

void viewInitiateGameWindow();

void viewDrawText(const char text[], float x, float y);

void viewClearWindow();

void viewDrawDefaultBoard();

void viewDrawObject(elementModel object);

void viewDrawPlayerName(const char name[PLAYER_NAME_BUFFER]);

void viewDrawScore(int score);

void viewDrawTime(int time);

#endif