#ifndef _MODEL_H_
#define _MODEL_H_

#include <time.h>
#include <stdlib.h>

#include "view_console.h"

#define FIELD_WIDTH_BX 10
#define FIELD_HEIGHT_BX 21
#define NUM_MOVING_ELEMENTS 3

#define NUM_COLOURS 5

#define OBJECT_TYPE_CIRCLE 1
#define OBJECT_TYPE_RECTANGLE 2
#define OBJECT_TYPE_TRIANGLE 3
#define OBJECT_TYPE_4 4
#define OBJECT_TYPE_5 5


#define MIRROR_STATE_LEFT 1
#define MIRROR_STATE_RIGHT 2

#define TRUE 1
#define FALSE 0

typedef struct elementModels {
    int colour;
    int type;
    int x;
    int y;
    int mark_for_deletion;
} elementModel;

typedef struct movingElementModels {
    elementModel cluster[NUM_MOVING_ELEMENTS];
    int mark_for_deletion;
} movingElementModel;
    
struct Board {
    elementModel field[FIELD_HEIGHT_BX][FIELD_WIDTH_BX];
    movingElementModel moving_element;
};

struct Game {
    int state;
    int score;
    int game_time;
    char player_name[PLAYER_NAME_BUFFER];
    struct Board board;
};

void modelGenerateNewHead(struct Game *game_model, unsigned seed);

void modelHeadMoveDown(movingElementModel *head);

void modelHeadMoveRight(movingElementModel *head);

void modelHeadMoveLeft(movingElementModel *head);

void modelHeadFlip(movingElementModel *head);

void modelElementMirror(movingElementModel *current_element);

void modelHeadMakeStatic(struct Game *target_game);

int modelMarkLinks(struct Game *game_model);

void modelRemoveMarkedBalls(struct Game *game_model);

void modelResetGame(struct Game *game_model);

int modelUpdateElements(struct Game *game_model);

#endif