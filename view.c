#include "view.h"

#define PI 3.141


#define NUM_BOXES_IN_BOARD 25
#define NUM_V_LINES_IN_BOARD (NUM_BOXES_IN_BOARD + 2)
#define BOX_H_SIZE_PX (VIEW_WIDTH/NUM_BOXES_IN_BOARD)
#define BOX_V_SIZE_PX (VIEW_HEIGHT/NUM_BOXES_IN_BOARD)
#define BOX_CENTER_X (BOX_H_SIZE_PX / 2)
#define BOX_CENTER_Y (BOX_V_SIZE_PX / 2)
#define LOWER_VIEW_BOUND VIEW_HEIGHT
#define UPPER_VIEW_BOUND 0.00
#define RIGHT_VIEW_BOUND VIEW_WIDTH
#define LEFT_VIEW_BOUND 0.00

#define FIELD_OFFSET_BX 3
#define FIELD_INVISIBLE_BOXES 2
#define FIELD_X1 BOX_H_SIZE_PX - 2.00
#define FIELD_Y1 (BOX_V_SIZE_PX * FIELD_OFFSET_BX)
#define FIELD_X2 (FIELD_X1 + (BOX_H_SIZE_PX * FIELD_WIDTH_BX))
#define FIELD_Y2 (FIELD_Y1 + (BOX_V_SIZE_PX * (FIELD_HEIGHT_BX - FIELD_INVISIBLE_BOXES)))

#define FIELD_INNER_BORDER_WIDTH 4

#define FIELD_INNER_BORDER_COLOUR al_map_rgb(40, 40, 40)
#define GRIDLINE_COLOUR al_map_rgb(70, 70, 70)
#define FIELD_GRID_COLOUR al_map_rgb(11, 11, 11)

#define INFORMATION_BOX_START_X
#define INFORMATION_BOX_START_Y

#define INFORMATION_BOX_WIDTH 7

void pViewDrawInfoBox() {
    al_draw_filled_rectangle(FIELD_X2 + BOX_H_SIZE_PX, FIELD_Y1,
            FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX), FIELD_Y2,
            LS_allegro_get_color(BLACK));
}

void viewDrawPlayerName(const char name[PLAYER_NAME_BUFFER]) {

    viewDrawText("Player", FIELD_X2 + BOX_H_SIZE_PX +  (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
            FIELD_Y1 + BOX_H_SIZE_PX);

    viewDrawText(name, FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
            FIELD_Y1 + +BOX_H_SIZE_PX + 20);
}

void viewDrawScore(int score) {

        viewDrawText("Score", FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
                FIELD_Y1 + BOX_H_SIZE_PX + 2 * BOX_V_SIZE_PX);

        al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE),
                      FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
                      FIELD_Y1 + +BOX_H_SIZE_PX + 20 + 2 * BOX_V_SIZE_PX, ALLEGRO_ALIGN_CENTER,"%d", score);
}

void viewDrawTime(int time) {

        viewDrawText("Time", FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
                FIELD_Y1 + BOX_H_SIZE_PX + 4 * BOX_V_SIZE_PX);

        al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE),
                      FIELD_X2 + BOX_H_SIZE_PX + (INFORMATION_BOX_WIDTH * BOX_H_SIZE_PX / 2),
                      FIELD_Y1 + +BOX_H_SIZE_PX + 20 + 4 * BOX_V_SIZE_PX, ALLEGRO_ALIGN_CENTER,"%d", time);
}

void viewDrawField(struct Game *game_model) {
    int i = 0, j = 0;

    for (i = 0; i < FIELD_HEIGHT_BX; i++) {
        for (j = 0; j < FIELD_WIDTH_BX; j++) {
            if (game_model->board.field[i][j].colour != 0) {
                viewDrawObject(game_model->board.field[i][j]);
            }
        }
    }

}

void viewInitiateGameWindow() {
    //Iniciating Allegro
	LS_allegro_init(VIEW_WIDTH, VIEW_HEIGHT,"Serbian Jewls");
}

void viewDrawText(const char text[], float x, float y) {
    al_draw_textf(LS_allegro_get_font(NORMAL),LS_allegro_get_color(WHITE),x,y, ALLEGRO_ALIGN_CENTER,"%s", text);
}

void viewClearWindow() {
    LS_allegro_clear_and_paint(BLACK);
}

//TODO void pViewDrawGridlines()

//TODO void viewDrawBlankBackground()

void viewDrawObject(elementModel object) {

    switch(object.colour) {
        case 1:
            al_draw_filled_circle((float) ((BOX_CENTER_X + (BOX_H_SIZE_PX * object.x))),
                                  (float) (BOX_CENTER_Y + (BOX_V_SIZE_PX * object.y)),
                                  (float) ((BOX_H_SIZE_PX / 2) - 2),
                                  al_map_rgb(0, 255, 159));
            break;
        case 2:
            al_draw_filled_circle((float) ((BOX_CENTER_X + (BOX_H_SIZE_PX * object.x))),
                                  (float) (BOX_CENTER_Y + (BOX_V_SIZE_PX * object.y)),
                                  (float) ((BOX_H_SIZE_PX / 2) - 2),
                                  al_map_rgb(0, 184, 255));
            break;
        case 3:
            al_draw_filled_circle((float) ((BOX_CENTER_X + (BOX_H_SIZE_PX * object.x))),
                                  (float) (BOX_CENTER_Y + (BOX_V_SIZE_PX * object.y)),
                                  (float) ((BOX_H_SIZE_PX / 2) - 2),
                                  al_map_rgb(189, 0, 255));
            break;
        case 4:
            al_draw_filled_circle((float) ((BOX_CENTER_X + (BOX_H_SIZE_PX * object.x))),
                                  (float) (BOX_CENTER_Y + (BOX_V_SIZE_PX * object.y)),
                                  (float) ((BOX_H_SIZE_PX / 2) - 2),
                                  al_map_rgb(214, 0, 159));
            break;
        case 5:
            al_draw_filled_circle((float) ((BOX_CENTER_X + (BOX_H_SIZE_PX * object.x))),
                                  (float) (BOX_CENTER_Y + (BOX_V_SIZE_PX * object.y)),
                                  (float) ((BOX_H_SIZE_PX / 2) - 2),
                                  al_map_rgb(0, 30, 255));
            break;
        default:
            break;
    }
}

void pViewDrawInfoBar() {

    //TODO print the rectangle targetted to screen width - box h size
    //TODO print the player name using al_draw_textf
    //TODO print controls
}

void pViewDrawDefaultField() {
    int i = 0;
    int gridline_v_position, gridline_h_position;
    int field_width_px = FIELD_WIDTH_BX * BOX_H_SIZE_PX;


    al_draw_filled_rectangle(FIELD_X1, FIELD_Y1, FIELD_X2, FIELD_Y2, LS_allegro_get_color(BLACK));

    for (i = 0; i < FIELD_HEIGHT_BX - 1; i++) {
        //y position
        gridline_h_position = (int)((BOX_V_SIZE_PX * i) + (FIELD_OFFSET_BX * BOX_V_SIZE_PX));

        if (i == 0 || i == FIELD_HEIGHT_BX - 2) {
            al_draw_line(FIELD_X1, gridline_h_position, FIELD_X1 + field_width_px, gridline_h_position,
                         FIELD_INNER_BORDER_COLOUR, FIELD_INNER_BORDER_WIDTH);
        }

        else {
            //horizontal lines
            al_draw_line(FIELD_X1, gridline_h_position, FIELD_X1 + field_width_px, gridline_h_position,
                         FIELD_GRID_COLOUR, 1);
        }

    }

    for (i = 0; i < FIELD_WIDTH_BX + 1; i++) {

        //x position`
        gridline_v_position = (int)((BOX_H_SIZE_PX * i) + BOX_H_SIZE_PX);

        if (i == 0 || i == FIELD_WIDTH_BX) {
            //TODO GRIDLINES ON THE RIGHT CURRENTLY MISS ALIGN BY ~5 px
            al_draw_line(gridline_v_position - (FIELD_INNER_BORDER_WIDTH / 2.00), FIELD_Y1,
                         gridline_v_position - (FIELD_INNER_BORDER_WIDTH / 2.00), FIELD_Y2,
                         FIELD_INNER_BORDER_COLOUR, 5);
        }

        else {
            al_draw_line(gridline_v_position, FIELD_Y1, gridline_v_position, FIELD_Y2, FIELD_GRID_COLOUR, 1);
        }

    }
}


void viewDrawDefaultBoard() {
    int i = 0, gridline_v_position = 0, gridline_h_position;
    
    LS_allegro_clear_and_paint(DARK_BLUE);

    for(i = 0; i < NUM_V_LINES_IN_BOARD; i++) {

        gridline_v_position = (int) (BOX_H_SIZE_PX * (i + 1));
        gridline_h_position = (int) (BOX_V_SIZE_PX * (i + 1));
        
        al_draw_line(gridline_v_position, 0.00, gridline_v_position, LOWER_VIEW_BOUND, GRIDLINE_COLOUR, 1);
        al_draw_line(0.00, gridline_h_position, RIGHT_VIEW_BOUND, gridline_h_position, GRIDLINE_COLOUR, 1);
    }

    pViewDrawDefaultField();
    //al_draw_rectangle()
    pViewDrawInfoBox();
}
